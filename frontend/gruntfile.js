const webpackConfig = require('./webpack.config.js');

module.exports = function(grunt) {

  grunt.initConfig({

    clean: {
      options: {
        force: true
      },
      css: [
        'dist',
        '../public'
      ]
    },

    webpack: {
      default: webpackConfig
    },

    copy: {
      index: {
        files: [
          {
            src: 'src/index.html',
            dest: 'dist/index.html'
          }
        ]
      },
      all_to_public: {
        files: [
          {
            expand: true,
            cwd: 'dist',
            src: '**/*',
            dest: '../public'
          },
        ]
      }
    },

    replace: {
      time: {
        src: ['dist/index.html'],
        overwrite: true,
        replacements: [{
          from: '{time}',
          to: function() {
            return Date.now();
          }
        }]
      }
    },

    watch: {
      options: {
        spawn: false,
        livereload: true
      },
      js: {
        files: ['src/js/**/*.*', 'src/index.html'],
        tasks: ['webpack', 'copy']
      },
      scss: {
        files: ['src/scss/**/*.scss'],
        tasks: ['copy', 'webpack'],
        options: {
          livereload: false,
          spawn: true
        }
      },
      css: {
        files: [
          'dist/*.css'
        ],
        tasks: []
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-webpack');
  grunt.loadNpmTasks('grunt-text-replace');

  grunt.registerTask('default', [
    'clean',
    'webpack',
    'copy'
  ]);

  grunt.registerTask('prod', [
    'clean',
    'webpack',
    'copy:index',
    'replace',
    'copy:all_to_public'
  ]);
};
