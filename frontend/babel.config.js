module.exports = {
  presets: ['@babel/preset-env'],
  plugins: [
    [
      '@babel/transform-runtime',
      {
        corejs: {
          version: 3,
          proposals: true
        }
      }
    ]
  ]
};
