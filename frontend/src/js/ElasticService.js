export async function search(searchtext, page = 0) {
  const url = `${BACKEND_URL}/search?q=${encodeURIComponent(searchtext)}&p=${page}`
  const result = await fetch(url);
  return result.json();
}

